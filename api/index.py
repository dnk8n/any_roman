from flask import Flask

from any_roman import int_to_roman

app = Flask(__name__)


@app.route('/int_to_roman/<int:n>')
def get_int_to_roman(n):
    return int_to_roman(n)
